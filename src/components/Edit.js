import React, { Component } from 'react';

export default class Edit extends Component {

    render() {
        return (
            <div id="formulaireModification" className={this.props.visible ? 'col-md-4' : 'd-none'}>
                <div className="card">
                    <div className="card-body">
                        <form id="formulaireModificationform" onSubmit={this.props.onSubmit}>
                            <div className="form-group">
                                <label htmlFor="idInput">Id :</label> 
                                <input id="idInput" type="text" name="_id" defaultValue={this.props.id} required="required" placeholder="Id du restaurant à modifier" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="restaurantInput">Nom</label>
                                <input id="restaurantInput" type="text" name="nom" defaultValue={this.props.name} onChange={this.props.onNameChange} required="required" placeholder="Michel's restaurant" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="cuisineInput">Cuisine</label>
                                <input id="cuisineInput" type="text" name="cuisine" defaultValue={this.props.cuisine} onChange={this.props.onCuisineChange} required="required" placeholder="Michel's cuisine" className="form-control"/>
                            </div> 
                            <button className="btn btn-dark">Modifier ce restaurant</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

