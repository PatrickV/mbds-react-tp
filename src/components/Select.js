import React, { Component } from 'react';

export default class Select extends Component {

  render() {
    return (
      <select className="custom-select" id="elementPageDropDown" onChange={this.props.onChange}>
        <option value="5" select="true">5</option>
        <option value="10">10</option>
        <option value="15">15</option>
      </select>
    );
  }
}

