import React from 'react';

function Restaurant(props) {

    return (
      <tr id={props.id}>
        <td className="restaurant_name">{props.name}</td>
        <td className="restaurant_cuisine">{props.cuisine}</td>
        <td>
          <button className="btn btn-dark" onClick={() => props.editRestaurant(props.id)}><i className="fa fa-edit"></i></button>
          <button className="btn btn-dark" onClick={() => props.removeRestaurant(props)}><i className="fa fa-trash"> </i></button >
        </td>
      </tr>
    );
  }

  export default Restaurant;
