import React, {Component} from 'react';

export default class Alert extends Component {

    render() {
        if (this.props.hide) return null;
        return ( 
            <div class="alert alert-success" role="alert">
                <h6 class="alert-heading" > Opération réussie </h6> 
                <strong> Création </strong> d'un restaurant avec les parametres : {this.props.message} 
            </div>
        );
    }
}