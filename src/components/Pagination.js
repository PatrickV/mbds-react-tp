import React, { Component } from 'react';

export default class Pagination extends Component{
    render () {
        const paginationItems = [];
        const lastPage =  Math.ceil(this.props.totalElements / this.props.nbElemParPage);

        var currentPage = this.props.currentPage;

        if (currentPage > 2 && currentPage < lastPage) { 
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(currentPage - 1)}>{currentPage - 1}</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(currentPage)}>{currentPage}</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(currentPage + 1)}>{currentPage + 1}</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(lastPage)}>{lastPage}</button>)
        } else if (currentPage === 1 || currentPage === 2){
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(1)}>1</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(2)}>2</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(3)}>3</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(lastPage)}>{lastPage}</button>)
        } else if (currentPage === lastPage) {
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(1)}>1</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(lastPage - 2)}>{lastPage - 2}</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(lastPage - 1)}>{lastPage - 1}</button>)
            paginationItems.push(<button className="btn btn-dark" onClick={() => this.props.onClickButton(lastPage)}>{lastPage}</button>)
        }

        return (
            <div>
                {paginationItems[0]}
                &nbsp;
                {paginationItems[1]}
                &nbsp;
                {paginationItems[2]}
                &nbsp; ... &nbsp;
                {paginationItems[3]}
            </div>
        )
        
    }
}
