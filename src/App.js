import React, { Component } from 'react';
import './App.css';

import Restaurant from './components/Restaurant';
import Select from './components/Select';
import Alert from './components/Alert';
import Pagination from './components/Pagination';
import Edit from './components/Edit';

class App extends Component {
  constructor(props) {
    super(props);

		this.state = {
      restaurants:[],
      count: 0,
      pageSize: 5,
      currentPage: 1,
      restaurant_name: "",
      restaurant_cuisine: "",
      message: "",
      timerDone: true,
      search: "",
      edit: false,
      add: false
		}
  }

  addRestaurant = () => {
    var toInsert = {
      name: this.state.restaurant_name,
      cuisine: this.state.restaurant_cuisine
    }

		let oldRestaurants = this.state.restaurants;
		this.setState({
			restaurants: oldRestaurants.concat(toInsert)
    });

    fetch('http://localhost:8080/api/restaurants', {
      method: 'POST',
      headers: {'Content-Type':'application/json'},
      body: JSON.stringify({
        "nom": this.state.restaurant_name,
        "cuisine": this.state.restaurant_cuisine
      })
    })
      .then(response => {
       return response.json(); // transforme le json texte en objet js
      })
      .then(data => { // data c'est le texte json de response ci-dessus

        var message = " nom = " + toInsert.name + ", cuisine = " + toInsert.cuisine;
        
        this.setState({
          name: "",
          cuisine: "",
          timerDone: false,
          message: message,
          add: false,
        });

        setTimeout(() => {
          this.setState({
            timerDone: true
          })
        }, 3000);

      }).catch(err => {
        console.log("erreur dans le get : " + err)
    });
  }
  
  removeRestaurant(restaurant) {
	  const oldRestaurants = this.state.restaurants.filter(
      (elem, index) => {
        return (elem !== restaurant) ? elem : null;
      }
    );

		this.setState({
			restaurants: oldRestaurants
		});
  }
  
  editRestaurant(restaurant) {
    var restaurant_restaurant = document.getElementById(restaurant)
    var restaurant_name = restaurant_restaurant.childNodes[0].textContent;
    var restaurant_cuisine = restaurant_restaurant.childNodes[1].textContent;
    var restaurant_id = restaurant;

    this.setState({
      edit: true,
      add: false,
      restaurant_name: restaurant_name,
      restaurant_cuisine: restaurant_cuisine,
      restaurant_id: restaurant_id
    });
  }

  getDataFromServer() {
    console.log("--- GETTING DATA ---");
    var url = 'http://localhost:8080/api/restaurants?page=' + this.state.currentPage + '&pagesize=' + this.state.pageSize;
    if (this.state.search !== "") {
      url = 'http://localhost:8080/api/restaurants?page=' + this.state.currentPage + '&pagesize=' + this.state.pageSize + '&name=' + this.state.search;
    }
    fetch(url)
    .then(response => {
      return response.json(); // transforme le json texte en objet js
    })
    .then(data => { // data c'est le texte json de response ci-dessus
      let newRestaurants = [];
      data.data.forEach((el) => {
        newRestaurants.push(el)
      });

      this.setState({
      restaurants: newRestaurants
    });

    }).catch(err => {
      console.log("erreur dans le get : " + err)
    });
  }

  getDataCountFromServer() {
    console.log("--- GETTING DATA COUNT---");
    fetch('http://localhost:8080/api/restaurants/count')
      .then(response => {
        return response.json(); // transforme le json texte en objet js
      })
      .then(data => { // data c'est le texte json de response ci-dessus
        let count = data.data;

        this.setState({
          count: count
        });

      }).catch(err => {
        console.log("erreur dans le get : " + err)
      });

  }

  handleSizeChange = (e) => {
    this.setState({
      pageSize: e.target.value
    }, () => {
      fetch('http://localhost:8080/api/restaurants?page=' + this.state.currentPage + '&pagesize=' + this.state.pageSize)
        .then(response => {
          return response.json(); // transforme le json texte en objet js
        })
        .then(data => { // data c'est le texte json de response ci-dessus
          let newRestaurants = [];
          data.data.forEach((el) => {
            newRestaurants.push(el)
          });

          this.setState({
            restaurants: newRestaurants
          });

        }).catch(err => {
          console.log("erreur dans le get : " + err)
        });
    });
  }

  handleClickButtonAdd = (e) => {
    this.setState({
      add: true,
      edit: false
    });
  }

  handleClickButtonPage = (currentPage) => {
    this.setState({
      currentPage: currentPage
    }, () => {
      this.getDataFromServer()
    });
  }

  handleChangeSearch = (e) => {
    this.setState({
      search: e.target.value
    }, () => {
      this.getDataFromServer()
    });
  }

  handleNameChange = (e) => {
    this.setState({
      restaurant_name: e.target.value
    });
  }

  handleCuisineChange = (e) => {
    this.setState({
      restaurant_cuisine: e.target.value
    });
  }

  handleEditRestaurant = (e) => {
    const data = new FormData();
    data.append('_id', this.state.restaurant_id);
    data.append('nom', this.state.restaurant_name);
    data.append('cuisine', this.state.restaurant_cuisine);

    // Mettre le put
    fetch('http://localhost:8080/api/restaurants/' + this.state.restaurant_id, {
      method: 'PUT',
      body: data
    })
      .then(response => {
        return response.json(); // transforme le json texte en objet js
      })
      .then(data => { // data c'est le texte json de response ci-dessus

      }).catch(err => {
        console.log("erreur dans le get : " + err)
      });
  }

  componentWillMount() {
    console.log("Component will mount");
    // on va chercher des donnees sur le Web avec fetch, comme
    // on a fait avec VueJS
    this.getDataFromServer();
    this.getDataCountFromServer();
  }

  render() {
    console.log("render");
    let listAvecComponent =
				this.state.restaurants.map((el, index) => {
				return <Restaurant
          name={el.name}
          cuisine={el.cuisine}
          id={el._id}
          key={index}
          editRestaurant={this.editRestaurant.bind(this)}
          removeRestaurant={this.removeRestaurant.bind(this)}/>
			}
    );

    return (
      <div className="container" >
        <br />
        <h2> Tables des restaurants </h2> 

        <Alert message={this.state.message} hide={this.state.timerDone}/>

        <button type="button" className="btn btn-dark mb-3" onClick={this.handleClickButtonAdd} id="createButton"><i className="fa fa-plus"></i></button >
        <div className="row">
          <div className={this.state.add || this.state.edit ? 'col-md-8' : 'col-md-12'}>
            <div className="input-group mb-3">
              <div className="input-group-prepend">
                <label className="input-group-text" htmlFor="elementPageDropDown"> Elements par page </label> 
              </div> 
              <Select onChange={this.handleSizeChange}/>
            </div> 
            <div className="input-group mb-3" >
              <input id="searchInput" className="form-control" onChange={this.handleChangeSearch} placeholder="Chercher par nom" v-model = "query" />
            </div> 
            <table className="table table-bordered" id="myTable">
              <thead className="thead-dark" >
                <tr>
                  <th>Nom</th> 
                  <th>Cuisine</th> 
                  <th>Action</th> 
                </tr> 
              </thead> 
              <tbody>
                {listAvecComponent}
              </tbody>
            </table> 
          </div>
          <div className={this.state.add ? 'col-md-4' : 'd-none'} id="formulaireInsertion" >
            <div className="card" >
              <div className="card-body" >
                <form id="formulaireInsertionform" onSubmit={this.handleSubmit}>
                  <div className="form-group" >
                    <label htmlFor="restaurantInput" > Nom </label> 
                    <input className="form-control" id="restaurant-input" type="text" name="nom" required placeholder="Michel's restaurant" value={this.state.restaurant_name} onChange={this.handleNameChange}/>
                  </div> 
                  <div className="form-group">
                    <label htmlFor="cuisineInput" > Cuisine </label> 
                    <input className="form-control" id="cuisine-input" type="text" name="cuisine" required placeholder="Michel's cuisine" value={this.state.restaurant_cuisine} onChange={this.handleCuisineChange}/>
                  </div>
                  <button className="btn btn-dark" type="button" onClick={this.addRestaurant}> Créer restaurant </button> 
                </form> 
              </div> 
            </div> 
          </div>
          <Edit 
            visible={this.state.edit}
            name={this.state.restaurant_name} 
            cuisine={this.state.restaurant_cuisine} 
            id={this.state.restaurant_id}
            onSubmit={this.handleEditRestaurant}
            onNameChange={this.handleNameChange}
            onCuisineChange={this.handleCuisineChange}></Edit>
        </div> 
        <div className="navigation">
          <Pagination 
            onClickButton={this.handleClickButtonPage}
            totalElements={this.state.count}
            nbElemParPage={this.state.pageSize}
            currentPage={this.state.currentPage}></Pagination>
        </div>
      </div>
    );
  }
}

export default App;
